(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name funcef-message
    * @version 1.0.0
    * @Componente para apresentação de alertas e confirmações
    */
    angular.module('funcef-message.service', []);
    angular.module('funcef-message.controller', []);
    angular.module('funcef-message.configuration', []);

    angular
        .module('funcef-message', [
            'ui-notification',
            'ui.bootstrap',
            'funcef-message.service',
            'funcef-message.controller',
            'funcef-message.configuration'
        ]);
})();;(function () {
    'use strict';

    angular.module('funcef-message.configuration')
    .config(['NotificationProvider', function (NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 5000,
            startTop: 60,
            startRight: 10,
            verticalSpacing: 10,
            horizontalSpacing: 10,
            positionX: 'right',
            positionY: 'top'
        });
    }]);
})();;(function () {
    'use strict';

    angular.module('funcef-message.controller').
        controller('NgfAlertaModalController', NgfAlertaModalController);

    NgfAlertaModalController.$inject = ['$uibModalInstance', 'title', 'message'];

    function NgfAlertaModalController($uibModalInstance, title, message) {
        var vm = this;
        vm.confirm = confirm;
        vm.title = title;
        vm.message = message;

        ////////////

        function confirm() {
            $uibModalInstance.close(true);
        }
    }
})();;(function () {
    'use strict';

    angular.module('funcef-message.controller').
        controller('NgfConfirmacaoModalController', NgfConfirmacaoModalController);

    NgfConfirmacaoModalController.$inject = ['$uibModalInstance', 'title', 'message'];

    function NgfConfirmacaoModalController($uibModalInstance, title, message) {
        var vm = this;
        vm.confirm = confirm;
        vm.close = close;
        vm.title = title;
        vm.message = message;

        ////////////

        function confirm() {
            $uibModalInstance.close(true);
        }

        function close() {
            $uibModalInstance.close(false);
        }
    }
})();;(function () {
    'use strict';

    angular.module('funcef-message.service')
        .service('$message', NgfMessageService);

    NgfMessageService.$inject = ['$uibModal','Notification'];

    /* @ngInject */
    function NgfMessageService($uibModal, Notification) {
        var service = {
            principal: principal,
            sucesso: sucesso,
            erro: erro,
            atencao: atencao,
            info: info,
            alerta: alerta,
            confirmacao: confirmacao
        };

        return service;

        ////////

        function principal(message, time, callback) {
            Notification.primary({ message: message, delay: time, onClose: callback });
        }

        function sucesso(message, time, callback) {
            Notification.success({ message: message, delay: time, onClose: callback });
        }

        function erro(message, time, callback) {
            Notification.error({ message: message, delay: time, onClose: callback });
        }

        function atencao(message, time, callback) {
            Notification.warning({ message: message, delay: time, onClose: callback });
        }

        function info(message, time, callback) {
            Notification.info({ message: message, delay: time, onClose: callback });
        }

        function alerta(title, message, callback) {
            $uibModal.open({
                templateUrl: 'views/alerta.modal.view.html',
                controller: 'NgfAlertaModalController',
                controllerAs: 'vm',
                resolve: {
                    title: function () { 
                        return title; 
                    },
                    message: function() { 
                        return message; 
                    }
               }
            }).result.then(function (confirm) {
                if (confirm && typeof callback === 'function') {
                    callback();
                }
            });
        }

        function confirmacao(title, message, callback) {
            $uibModal.open({
                templateUrl: 'views/confirmacao.modal.view.html',
                controller: 'NgfConfirmacaoModalController',
                controllerAs: 'vm',
                resolve: {
                    title: function () { 
                        return title; 
                    },
                    message: function() { 
                        return message; 
                    }
               }
            }).result.then(function (confirm) {
                if (confirm && typeof callback === 'function') {
                    callback();
                }
            });
        }
    };
}());
;angular.module('funcef-message').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/alerta.modal.view.html',
    "<div class=\"modal-header\"> <h4 class=\"modal-title\"> {{vm.title}} </h4> </div> <div class=\"modal-body\"> <div class=\"text-white-space\"> {{vm.message}} </div> </div> <div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-primary\" ng-click=\"vm.confirm()\"> <i class=\"fa fa-thumbs-o-up\"></i> Ok </button> </div>"
  );


  $templateCache.put('views/confirmacao.modal.view.html',
    "<div class=\"modal-header\"> <h4 class=\"modal-title\"> {{vm.title}} </h4> </div> <div class=\"modal-body\"> <div class=\"text-white-space\"> {{vm.message}} </div> </div> <div class=\"modal-footer\"> <button type=\"button\" class=\"btn btn-primary\" ng-click=\"vm.confirm()\"> <i class=\"fa fa-check\"></i> Confirmar </button> <button type=\"button\" class=\"btn btn-default\" ng-click=\"vm.close()\"> <i class=\"fa fa-close\"></i> Cancelar </button> </div>"
  );

}]);
