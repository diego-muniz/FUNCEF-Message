(function () {
    'use strict';

    angular.module('funcef-message.service')
        .service('$message', NgfMessageService);

    NgfMessageService.$inject = ['$uibModal','Notification'];

    /* @ngInject */
    function NgfMessageService($uibModal, Notification) {
        var service = {
            principal: principal,
            sucesso: sucesso,
            erro: erro,
            atencao: atencao,
            info: info,
            alerta: alerta,
            confirmacao: confirmacao
        };

        return service;

        ////////

        function principal(message, time, callback) {
            Notification.primary({ message: message, delay: time, onClose: callback });
        }

        function sucesso(message, time, callback) {
            Notification.success({ message: message, delay: time, onClose: callback });
        }

        function erro(message, time, callback) {
            Notification.error({ message: message, delay: time, onClose: callback });
        }

        function atencao(message, time, callback) {
            Notification.warning({ message: message, delay: time, onClose: callback });
        }

        function info(message, time, callback) {
            Notification.info({ message: message, delay: time, onClose: callback });
        }

        function alerta(title, message, callback) {
            $uibModal.open({
                templateUrl: 'views/alerta.modal.view.html',
                controller: 'NgfAlertaModalController',
                controllerAs: 'vm',
                resolve: {
                    title: function () { 
                        return title; 
                    },
                    message: function() { 
                        return message; 
                    }
               }
            }).result.then(function (confirm) {
                if (confirm && typeof callback === 'function') {
                    callback();
                }
            });
        }

        function confirmacao(title, message, callback) {
            $uibModal.open({
                templateUrl: 'views/confirmacao.modal.view.html',
                controller: 'NgfConfirmacaoModalController',
                controllerAs: 'vm',
                resolve: {
                    title: function () { 
                        return title; 
                    },
                    message: function() { 
                        return message; 
                    }
               }
            }).result.then(function (confirm) {
                if (confirm && typeof callback === 'function') {
                    callback();
                }
            });
        }
    };
}());
