(function () {
    'use strict';

    angular.module('funcef-message.controller').
        controller('NgfAlertaModalController', NgfAlertaModalController);

    NgfAlertaModalController.$inject = ['$uibModalInstance', 'title', 'message'];

    function NgfAlertaModalController($uibModalInstance, title, message) {
        var vm = this;
        vm.confirm = confirm;
        vm.title = title;
        vm.message = message;

        ////////////

        function confirm() {
            $uibModalInstance.close(true);
        }
    }
})();