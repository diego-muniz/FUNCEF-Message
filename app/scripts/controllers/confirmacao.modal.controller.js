(function () {
    'use strict';

    angular.module('funcef-message.controller').
        controller('NgfConfirmacaoModalController', NgfConfirmacaoModalController);

    NgfConfirmacaoModalController.$inject = ['$uibModalInstance', 'title', 'message'];

    function NgfConfirmacaoModalController($uibModalInstance, title, message) {
        var vm = this;
        vm.confirm = confirm;
        vm.close = close;
        vm.title = title;
        vm.message = message;

        ////////////

        function confirm() {
            $uibModalInstance.close(true);
        }

        function close() {
            $uibModalInstance.close(false);
        }
    }
})();