(function () {
    'use strict';

    angular.module('funcef-message.configuration')
    .config(['NotificationProvider', function (NotificationProvider) {
        NotificationProvider.setOptions({
            delay: 5000,
            startTop: 60,
            startRight: 10,
            verticalSpacing: 10,
            horizontalSpacing: 10,
            positionX: 'right',
            positionY: 'top'
        });
    }]);
})();