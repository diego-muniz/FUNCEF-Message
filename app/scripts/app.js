﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name funcef-message
    * @version 1.0.0
    * @Componente para apresentação de alertas e confirmações
    */
    angular.module('funcef-message.service', []);
    angular.module('funcef-message.controller', []);
    angular.module('funcef-message.configuration', []);

    angular
        .module('funcef-message', [
            'ui-notification',
            'ui.bootstrap',
            'funcef-message.service',
            'funcef-message.controller',
            'funcef-message.configuration'
        ]);
})();