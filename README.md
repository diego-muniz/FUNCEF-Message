# Componente - FUNCEF-Message

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Desinstalação](#desinstalação)

## Descrição

- Componente Message apresenta notificações ou telas de confirmação no topo da página com as informações de acordo com cada situação (Primary, Sucesso, Erro, Atenção, Informativo, Alerta e Confirmação).

## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-message --save.
```

## Script

```html
<script src="bower_components/funcef-message/dist/funcef-message.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-message/dist/funcef-message.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funcef-message dentro do módulo do Sistema.

```js
angular
        .module('funcef-demo', ['funcef-message']);
```

## Uso

##### Controller

```js
$message.alerta('Mensagem de Alerta!', 'Descrição do alerta');
$message.confirmacao('Título da confirmação', 'Texto da confirmação', confirmado);

function confirmado() {

}
```

## Desinstalação:

```
bower uninstall funcef-message --save
```
