﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = ['$message'];

    /* @ngInject */
    function Example1Controller($message) {
        var vm = this;
        vm.principal = principal;
        vm.sucesso = sucesso;
        vm.erro = erro;
        vm.atencao = atencao;
        vm.alerta = alerta;
        vm.info = info;
        vm.confirmacao = confirmacao;
        
        ////////

        function principal() {
            $message.principal('Mensagem Primária!');
        }

        function sucesso() {
            $message.sucesso('Mensagem de Sucesso!');
        }

        function erro() {
            $message.erro('Mensagem de Erro!');
        }

        function atencao() {
            $message.atencao('Mensagem de Alerta!');
        }

        function info() {
            $message.info('Mensagem de Informativo!');
        }

        function alerta() {
            $message.alerta('Mensagem de Alerta!', 'Texto da confirmação');
        }

        function confirmacao() {
            $message.confirmacao('Título da confirmação', 'Texto da confirmação', confirmado);
        }

        function confirmado() {
            $message.sucesso('Você confirmou!!!');
        }
    }
}());